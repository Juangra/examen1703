<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Book::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('es_ES');
    return [
        'title' => substr($faker->sentence, 0, 50),
        'pages' => rand(50, 500),
        'year' => rand(1800, 2017),
        'gender_id' =>  App\User::all()->random()->id,
        'user_id' => App\User::all()->random()->id,
    ];
});

$factory->define(App\Author::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('es_ES');
    return [
        'name' => $faker->name,
        'country' => $faker->country(),
        'birth_year' => rand(1750, 1990),
    ];
});


