@extends('layouts.app')

@section('content')

<h1>Crear registro</h1>
<div class="form">
<form action="/books" method="post">
    {{ csrf_field() }}

    <div class="form-group">
        <label>Título: </label>
        <input type="text" name="tittle" value="{{ old('tittle') }}">
        {{ $errors->first('tittle') }}
    </div>

    <div class="form-group">
        <label>Páginas: </label>
        <input type="text" name="pages" value="{{ old('pages') }}">
        {{ $errors->first('pages') }}
    </div>

    <div class="form-group">
        <label>Año: </label>
        <input type="text" name="year" value="{{ old('year') }}">
        {{ $errors->first('year') }}
    </div>


    <div class="form-group">
        <label>Genero: </label>
        <select type="select" name="gender_id" value="{{ old('gender_id') }}">
            @foreach ($genders as $gender)
            <option value="{{ $gender->id }}">{{ $gender->name }}</option>
            @endforeach
        </select>
        {{ $errors->first('gender_id') }}
    </div>


    <div class="form-group">
        <label>Usuario: </label>
        <input type="text" name="user_id" value="{{ $user_id }}" readonly>
        {{ $errors->first('user_id') }}
    </div>

    <div class="form-group">
        <input type="submit" value="Guardar">
    </div>
</form>
</div>

@endsection('content')