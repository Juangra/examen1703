@extends('layouts.app')
@section('content')

    <h1>Detalle de libro</h1>
    <p>Id: {{ $book->id }}</p>
    <p>Titulo: {{ $book->tittle }}</p>
    <p>Páginas: {{ $book->pages }}</p>
    <p>Año: {{ $book->year }}</p>
    <p>Genero: {{ $book->gender->name }}</p>
     @foreach ($book->authors as $author)
            <li>{{ $author->name }}</li>
    @endforeach
    <p>Autores: {{ $book->$authors->name }}</p>
    
    <br><a href="/books">Volver</a>

@endsection('content')