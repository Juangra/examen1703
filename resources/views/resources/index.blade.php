@extends('layouts.app')

@section('content')
    
    <h1>Libros</h1>
    
        <a href="/books/create">Nuevo</a>
   

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Titulo</th>
                <th>Páginas</th>
                <th>Año</th>
                <th>Género</th>
                <th>Usuario</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($books as $book)
            <tr>
                
                
                
                <td>  {{ $book->id }} </td>
                <td>  {{ $book->title }} </td>
                <td>  {{ $book->pages }} </td>
                <td>  {{ $book->year }} </td>
                <td>  {{ $book->gender->name }} </td>
                <td>  {{ $book->user->name }} </td>
                <td> 
                   

                   <form method="post" action="/books/{{ $book->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}

                       
                        <input type="submit" value="Borrar">
                        

                        
                        
                      
                        
                        <a href="/books/{{ $book->id }}"> Ver </a>
                       
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $books->render() }}

@endsection('content')