<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected $fillable = ['id', 'name'];

    public function books()
    {
        return $this->hasMany('App\Book');
    }
}
