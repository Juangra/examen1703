<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model{

    protected $fillable = ['id', 'pages', 'year', 'gender_id', 'user_id'];


    public function authors()
    {
        return $this->belongsToMany('App\Author');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function gender()
    {
        return $this->belongsTo('App\Gender');
    }
}
