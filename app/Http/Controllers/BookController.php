<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Gender;
use App\Author;
use Auth;


class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $user = $request->user();
        $books = Book::paginate(5);
        return view('resources.index', ['books' => $books]);
        
    }

    public function create()
    {
        //$this->authorize('create', Classbook::class);
        $books = Book::all();
        $gender_id = Gender::all();
        $user_id = Auth::user()->id;
        $genders = Gender::all();
        return view('resources.create', ['genders'=>$genders, 'gender_id' => $gender_id, 'user_id' => $user_id]);
    }
 
    public function store(Request $request)
    {
        $this->validate($request, [
        'tittle' => 'required|max:50',
        'pages' => 'numeric',
        'year' => 'numeric',
        'gender_id' => 'exists:genders,id',
        ]);

        $book->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/books');
        }
    }

    
    public function show($id, Request $request)
    {
        $book = Book::findOrFail($id);
        $authors = Author::all();
        
        if ($request->ajax()) {
            return $book;
        } else {
            return view('resources.show', ['book' => $book, 'authors' => $authors ]);
        }
    }

    
    
       
        

    public function destroy($id)
    {
        $book = Book::findOrFail($id);
       
        Book::destroy($id);
        return redirect('/books');
    }
}
