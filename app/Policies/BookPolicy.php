<?php

namespace App\Policies;

use App\User;
use App\Book;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClassbookPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        //return $user->isUser();
        return true;
    }

    public function update(User $user, Book $book)
    {
        //return $user->isUser();
        return true;
    }
    public function view(User $user, Book $book)
    {
        return true;
    }
    public function delete(User $user, Book $book)
    {
        //return $user->id === $book->user_id;
        return true;
        
    }

}

